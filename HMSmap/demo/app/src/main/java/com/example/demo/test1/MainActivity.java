package com.wz.android.mapdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapView;
import com.huawei.hms.maps.OnMapReadyCallback;

/**
 * Map activity entrance class.
 */
public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MapViewDemoActivity";
    // Huawei map.
    private HuaweiMap hMap;

    private MapView mMapView;

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain a MapView instance.
        mMapView = findViewById(R.id.mapView);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        // Please replace Your API key with the API key in
        // agconnect-services.json.
        MapsInitializer.setApiKey("CgB6e3x9c5foAr14CP88emeQTGyqpLSxt/MSpj+yj0xBAV+yWQQwNBCV7P2h+4iRQiRQkvpHtC5YAnNON+glkO/K");
        mMapView.onCreate(mapViewBundle);
        // Obtain a map instance.
        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(HuaweiMap map) {
        // Obtain a map instance from callback.
        Log.d(TAG, "onMapReady: ");
        hMap = map;
    }
}

//package com.example.demo.test1;
//
//import android.os.Bundle;
//import android.util.Log;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.huawei.hms.maps.HuaweiMap;
//import com.huawei.hms.maps.MapView;
//
//package com.wz.android.mapdemo;
//
//public class MainActivity extends AppCompatActivity implements OnMapReadyCallback{
//    private static final String TAG = "MapViewDemoActivity";
//    private SupportMapFragment mSupportMapFragment;
//
//    private HuaweiMap hMap;
//    private MapView mMapView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment_mapfragmentdemo);
//        mSupportMapFragment.getMapAsync(this);
//    }
//
//    @Override
//    public void onMapReady(HuaweiMap huaweiMap) {
//        Log.d(TAG, "onMapReady: ");
//        hMap = huaweiMap;
//    }
//}